package com.alvaro.corredores.modelo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;

@Entity
public class CorredorProfesional extends Corredor implements Serializable {

	private String nacionalidad;
	private String licencia;

	public CorredorProfesional() {
		super();
		this.nacionalidad = "";
		this.licencia = "";
	}

	public CorredorProfesional(String dni, String nombre, Date nacimiento, int tiempo, String nacionalidad,
			String licencia) {
		super(dni, nombre, nacimiento, tiempo);
		this.nacionalidad = nacionalidad;
		this.licencia = licencia;
	}

	public String getNacionalidad() {
		return nacionalidad;
	}

	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}

	public String getLicencia() {
		return licencia;
	}

	public void setLicencia(String licencia) {
		this.licencia = licencia;
	}

	public void vista() {

		System.out.println(toString());
	}

}

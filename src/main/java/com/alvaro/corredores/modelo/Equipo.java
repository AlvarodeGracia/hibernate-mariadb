package com.alvaro.corredores.modelo;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Equipos")
public class Equipo {

	@Id
	@Column(name = "nombre")
	private String nombre;

	@Column(name = "presupuesto")
	private float presupuesto;

	@OneToMany(mappedBy = "equipo", cascade = CascadeType.ALL)
	// NO Ordenada
	private Set<Corredor> corredores;

	// Ordenada
	// private List<Corredor> corredores;

	public Equipo() {
		super();
		this.nombre = "";
		this.presupuesto = 0;
		this.corredores = new HashSet<Corredor>();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public float getPresupuesto() {
		return presupuesto;
	}

	public void setPresupuesto(Float presupuesto) {
		this.presupuesto = presupuesto;
	}

	public Set<Corredor> getCorredores() {
		return corredores;
	}

	public void setCorredores(Set<Corredor> corredores) {
		this.corredores = corredores;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Equipo other = (Equipo) obj;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Equipo [nombre=" + nombre + ", presupuesto=" + presupuesto + ", corredores=" + corredores + "]";
	}

	public void vista() {

		System.out.println(toString());
	}

}

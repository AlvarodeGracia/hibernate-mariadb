package com.alvaro.corredores.modelo;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Entrenadores")
public class Entrenador {

	@Id
	public String dni;
	public String nombre;

	public Entrenador() {
		super();
		this.dni = "";
		this.nombre = "";
	}

	public Entrenador(String dni, String nombre) {
		super();
		this.dni = dni;
		this.nombre = nombre;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public String toString() {
		return "Entrenador [dni=" + dni + ", nombre=" + nombre + "]";
	}

	public void vista() {

		System.out.println(toString());
	}
}

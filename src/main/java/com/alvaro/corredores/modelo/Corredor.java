package com.alvaro.corredores.modelo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "Corredores")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Corredor implements Serializable {

	@Id
	@Column(name = "dni")
	protected String dni;

	@Column(name = "nombre")
	protected String nombre;

	@Column(name = "nacimiento")
	protected Date nacimiento;

	@Column(name = "tiempo")
	protected int tiempo;

	// Le indicamos que es una relacion 1:1
	// A la hora de como actuar ante aciones sobre la dependencia tenemos varias
	// opciones
	// --none -> no se realzia nada sobr corredor
	// --save-update -> si se inserta o actualzia el objeto Entrenador se hace
	// tambien en el objeto corredor
	// --delete -> si se borra entrenador se borra tambien corredor
	// --all
	@OneToOne(cascade = CascadeType.ALL)
	// La relaccion entre las dos tablas se realiza por la clave primaria
	@PrimaryKeyJoinColumn
	protected Entrenador entrenador;

	@ManyToOne
	@PrimaryKeyJoinColumn
	protected Equipo equipo;

	public Corredor() {

		dni = "";
		nombre = "";
		nacimiento = null;
		tiempo = 0;

	}

	public Corredor(String dni, String nombre, Date nacimiento, int tiempo) {
		super();
		this.dni = dni;
		this.nombre = nombre;
		this.nacimiento = nacimiento;
		this.tiempo = tiempo;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Date getNacimiento() {
		return nacimiento;
	}

	public void setNacimiento(Date nacimiento) {
		this.nacimiento = nacimiento;
	}

	public int getTiempo() {
		return tiempo;
	}

	public void setTiempo(int tiempo) {
		this.tiempo = tiempo;
	}

	public Entrenador getEntrenador() {
		return entrenador;
	}

	public void setEntrenador(Entrenador entrenador) {
		this.entrenador = entrenador;
	}

	public Equipo getEquipo() {
		return equipo;
	}

	public void setEquipo(Equipo equipo) {
		this.equipo = equipo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dni == null) ? 0 : dni.hashCode());
		return result;
	}
	
	

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Corredor other = (Corredor) obj;
		if (dni == null) {
			if (other.dni != null)
				return false;
		} else if (!dni.equals(other.dni))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Corredor [dni=" + dni + ", nombre=" + nombre + ", nacimiento=" + nacimiento + ", tiempo=" + tiempo
				+ ", entrenador=" + entrenador + ", equipo=" + equipo.getNombre() + "]";
	}

	public void vista() {

		System.out.println(toString());
	}

}

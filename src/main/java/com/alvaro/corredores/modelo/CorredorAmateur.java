package com.alvaro.corredores.modelo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;


@Entity
public class CorredorAmateur extends Corredor implements Serializable {

	@Column(nullable = false, length = 255)
	public String lugarNacimiento;

	public CorredorAmateur() {
		super();
		this.lugarNacimiento = "";
	}

	public CorredorAmateur(String dni, String nombre, Date nacimiento, int tiempo, String lugarNacimiento) {
		super(dni, nombre, nacimiento, tiempo);
		this.lugarNacimiento = lugarNacimiento;
	}

	public String getLugarNacimiento() {
		return lugarNacimiento;
	}

	public void setLugarNacimiento(String lugarNacimiento) {
		this.lugarNacimiento = lugarNacimiento;
	}

	public void vista() {

		System.out.println(toString());
	}

}

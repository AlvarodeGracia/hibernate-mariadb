package com.alvaro.corredores.orm;

import java.util.Properties;

//Imports
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

//Singleton
/*
 * Esta clase es singleton para que solo tengamos una instancia de la factoria de sesiones
 */
public class Hibernate {

	private static Hibernate instance = null;

	private SessionFactory sessionFactoryObj;

	public static Hibernate getInstance() {

		if (instance == null) {
			instance = new Hibernate();
		}

		return instance;
	}

	private Hibernate() {

		// se crea el objeto que configura hibernate
		// Utiliza un archivo XML al que se le establecen las diferentes propiedades
		// que necesita el session factory
		Configuration configObj = new Configuration();
		// Se lee el fichero de configuracion de hibernate
		configObj.configure("hibernate.cfg.xml");

		// Hibernate funciona por servicios y tenemos varias clase-servicio que trae los
		// diferentes servicios para utilizar
		// El mas comun es StandardServiceRegistry
		// Para crear un StandarServiceRegistruy se necesita un
		// StandardServiceRegistryBuilder
		// Este tipo de clase-servicio contiene la mayoria de servicios de hibernate,
		// estos son:
		// --Servicio de conexion: ConnectionProvider (normal)
		// MultiTenantConnectionProvider (multiples clientes)
		// --JdbcServices
		// --TransactionFactory
		// --JtaPlatform
		// --RegionFactory
		// --SessionFactory
		// --ServiceRegistryFactory
		StandardServiceRegistryBuilder standardServiceRegistryBuilder = new StandardServiceRegistryBuilder();

		// Cogemos del Configuration sus propiedades
		Properties properties = configObj.getProperties();
		System.out.println(properties.toString());

		// Aplica las propiedades al builder, para que sean iguales que el del
		// configuration
		standardServiceRegistryBuilder = standardServiceRegistryBuilder.applySettings(properties);

		// Construimos un objeto de la clase StandardServiceRegistry
		StandardServiceRegistry standardServiceRegistry = standardServiceRegistryBuilder.build();

		// Una vez hemos creado la clase-servicio con las mismas propiedades
		// establecidas en el Configuration
		// creamos una factoria de sesiones que suaremos para hacer las acciones contra
		// la BBDD
		sessionFactoryObj = configObj.buildSessionFactory(standardServiceRegistry);

	}

	public SessionFactory getSessionFactory() {

		return sessionFactoryObj;

	}

	public void close() {

		sessionFactoryObj.close();

	}

}

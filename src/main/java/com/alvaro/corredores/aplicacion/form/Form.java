package com.alvaro.corredores.aplicacion.form;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

public class Form {

	@SuppressWarnings("rawtypes")
	private Class clase;
	private Object modelo;

	private ArrayList<Input> inputs;
	private ArrayList<Object> ids;

	//Este constructor es para generar formularios que solo comprueba que un campo ID esta en la BBDD
	public Form(Object object) {

		System.out.println("Creando Formulario");
		this.modelo = object;
		this.clase = object.getClass();

		System.out.println(clase.getName());

		inputs = new ArrayList<Input>();
		ids = null;
	}
	
	//Si quieres que el formulario a parte de comrpobar que esta en la BBDD el id del objeto
		//Puedes pasarle otra lista con los ids que ya estan escogidos.
		public Form(Object object, ArrayList ids) {

			this(object);
			this.ids = ids;
		}

	public void ejecutarForm() {

		for (Input input : inputs) {
			do {

				input.pedirUsuario();

			} while (!validarInput(input));

		}
		System.out.println("Formulario completado");

	}

	public void addInput(Input input) {

		inputs.add(input);
	}

	public Object getModelo() {

		for (Input input : inputs) {
			String field = input.getVariable();
			field = field.substring(0, 1).toUpperCase() + field.substring(1);
			Method metodo;
			try {
				metodo = modelo.getClass().getMethod("set" + field, input.getEntrada().getClass());
				metodo.invoke(modelo, input.getEntrada());
			} catch (NoSuchMethodException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException e) {
				e.printStackTrace();
			}
		}
		return modelo;
	}

	private boolean validarInput(Input input) {

		boolean isCorrecto = true;
		Field field = null;

		try {
			field = clase.getDeclaredField(input.getVariable());

		} catch (NoSuchFieldException e) {

			// Si no encuntra ese campo puede ser que sea por que este en la superclase
			// Con esto lo vamos a buscar a la superclase
			try {
				if (clase.getSuperclass() != null)
					field = clase.getSuperclass().getDeclaredField(input.getVariable());

			} catch (NoSuchFieldException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (SecurityException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Annotation[] annotations = field.getDeclaredAnnotations();
		InputValidator validator;
		if(ids != null) {
			 validator = new InputValidator(annotations, input.getTipo(), input.getEntrada(), modelo.getClass(), ids);
		}else {
			 validator = new InputValidator(annotations, input.getTipo(), input.getEntrada(), modelo.getClass());
		}
		
		isCorrecto = validator.comprobar();

		return isCorrecto;

	}

}

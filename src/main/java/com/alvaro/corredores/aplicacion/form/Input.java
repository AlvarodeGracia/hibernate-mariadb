package com.alvaro.corredores.aplicacion.form;

import java.util.Calendar;
import java.util.Scanner;

public class Input {

	public enum InputType {

		ENTERO, DECIMAL, TEXTO, FECHA, LOGICO

	}

	private String variable;
	private String texto;
	private Object entrada;
	private InputType tipo;

	private boolean error;

	public Input(String variable, String texto, InputType tipo) {

		this.variable = variable;
		this.texto = texto;
		this.tipo = tipo;

		error = false;

	}

	private void mostrarTexto() {

		System.out.println(texto);
		
	}

	private void entradaUsuario() {

		switch (tipo) {
		case ENTERO:
			entrada = (new Scanner(System.in)).nextInt();
			break;
		case DECIMAL:
			entrada = (new Scanner(System.in)).nextFloat();
			break;
		case TEXTO:
			entrada = (new Scanner(System.in)).nextLine();
			break;
		case FECHA:// Entrada Fecha
			inputFecha();
			break;
		case LOGICO:// Entrada Booleana
			inputLogic();
			break;

		}
	}

	private void inputLogic() {

		String valor = "";
		while (!(valor.equals("true") || valor.equals("false"))) {
			System.out.println("true/false");
			valor = (new Scanner(System.in)).nextLine();
			if (valor.equalsIgnoreCase("true")) {
				entrada = true;
			} else if (valor.equalsIgnoreCase("false")) {
				entrada = false;
			}
		}
	}

	private void inputFecha() {

		int day, month, year;
		System.out.println("Inserte el year");
		year = (new Scanner(System.in)).nextInt();

		do {
			System.out.println("Inserte el mes");
			month = (new Scanner(System.in)).nextInt();
		} while (month <= 0 || month > 12);

		boolean dayFebrero = false;
		boolean day31 = false;
		boolean day30 = false;
		do {

			System.out.println("Inserte el dia");
			day = (new Scanner(System.in)).nextInt();

			// Comprobamos si el mes es febrero y tiene mas de 28 dias
			// Comprobamos si los meses son de mas de 30 dias o de 31
			dayFebrero = (month == 2 && day > 28);
			day31 = ((month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12)
					&& day > 31);
			day30 = ((month == 4 || month == 6 || month == 9 || month == 11) && day > 30);

		} while (day <= 0 || dayFebrero || day31 || day30);

		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.MONTH, month);
		cal.set(Calendar.DAY_OF_MONTH, day);
		entrada = cal.getTime();
	}

	public void pedirUsuario() {

		mostrarTexto();
		entradaUsuario();
		
	}

	public String getVariable() {
		return variable;
	}

	public void setVariable(String variable) {
		this.variable = variable;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public Object getEntrada() {
		return entrada;
	}

	public void setEntrada(Object entrada) {
		this.entrada = entrada;
	}

	public InputType getTipo() {
		return tipo;
	}

	public void setTipo(InputType tipo) {
		this.tipo = tipo;
	}

}

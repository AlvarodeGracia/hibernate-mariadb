package com.alvaro.corredores.aplicacion.form;

import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.util.ArrayList;

import javax.persistence.Column;

import com.alvaro.corredores.aplicacion.form.Input.InputType;
import com.alvaro.corredores.dao.DAO;
import com.alvaro.corredores.dao.DAOFactory;
import com.alvaro.corredores.orm.ORM;

public class InputValidator {

	private InputType tipo;
	private Object object;
	private Class<?> clase;
	private ArrayList ids;

	private boolean isID;
	// El input no puede estar vacio
	private boolean isNotNull;

	// El texto no puede tener mas de x caratceres
	private boolean isMaxSize;
	private int maxSize;

	// El numero no puede ser mayor de X
	private boolean isMax;
	private int max;

	// El numero no puede ser menor de X
	private boolean isMin;
	private int min;
	
	public InputValidator(Annotation[] annotations, InputType tipo, Object object) {
		this.tipo = tipo;
		this.object = object;
		
		for (Annotation annotation : annotations) {

			if (annotation.annotationType().getSimpleName().equals("Id")) {
				//Un id no puede ser null
				isNotNull = true;
				//Para marcar que es un id y comprobar que no este repetido
				isID = true;
			}

			//Si lleva una anotacion column con el valor nullable en false entonces no podra ser null
			if (annotation.annotationType().getSimpleName().equals("Column")) {
				Column column = (Column) annotation;
				if (!column.nullable()) {
					isNotNull = true;
				}
			}

		}
	}
	
	public InputValidator(Annotation[] annotations, InputType tipo, Object object,  ArrayList ids) {
		this(annotations, tipo, object);
		this.ids = ids;
	}

	public InputValidator(Annotation[] annotations, InputType tipo, Object object,  Class<?> clase) {
		this(annotations, tipo, object);
		this.clase = clase;
	}
	
	public InputValidator(Annotation[] annotations, InputType tipo, Object object,  Class<?> clase, ArrayList ids) {
		this(annotations, tipo, object);
		this.clase = clase;
		this.ids = ids;
	}

	public boolean comprobar() {

		boolean isCorrecto = true;

		switch (tipo) {

		case TEXTO:
			isCorrecto = comporbarTexto();
			break;
		case DECIMAL:
			break;
		case ENTERO:
			break;
		case FECHA:
			break;
		case LOGICO:
			break;
		default:
			break;

		}

		return isCorrecto;

	}

	private boolean comporbarTexto() {

		boolean isCorrecto = true;

		String texto = (String) object;
		
		if (isNotNull) {
			if (texto == null) {
				isCorrecto = false;
				System.out.println("Este campo no puede estar vacio");
			} else if (texto.equals("")) {
				isCorrecto = false;
				System.out.println("Este campo no puede estar vacio");
				
			}else if(isID) {
				
				//Si tiene la etiqueta ID significa que es clave primaria lo que conlleva
				//Que este campo no puede ser null, que ya se ha comprobado y que a la vez
				//no esta ya registrado en la BBDD
				
				if(clase != null) {
					//Cojemos el DAO para comprobar que exite el elemento
					DAO<Object, Serializable> dao = (new DAOFactory()).getDao(ORM.orm, clase);
					//Lo buscamos
					Object o = dao.findByID(texto);
					
					//Y vemos si nos ha devuelto algo, si es asi, ponemos el imput como errono para volver a insertar 
					if(o != null) {
						isCorrecto = false;
						System.out.println("Ya esta ese id en la BBDD");
					}
				}
				
				//A veces a parte de comprobar en una BBDD puede que tengamos una lista de IDS
				//Si se la hemos pasado al formulario cuando llegue a esta aprte comprobara que este id
				//no este en esa lista
				if(ids != null) {
					
					for(Object o : ids) {
						
						if(o.equals(texto)) {
							isCorrecto = false;
							System.out.println("Ya ha insertado ese id");
						}
					}
						
					
				}
			} 
		}
		
		
		return isCorrecto;
	}

}

package com.alvaro.corredores.aplicacion;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

import com.alvaro.corredores.aplicacion.opcionesMenu.DarBajaEquipo;
import com.alvaro.corredores.aplicacion.opcionesMenu.InsertarEquipo;
import com.alvaro.corredores.aplicacion.opcionesMenu.ModificarNacimiento;

/**
 * 
 * @author Alvaro de Gracia Pastor
 * 
 *         Esta clase es la encargada mostrar un menu y realizar la accion del
 *         metodo ejecutar de la interfaz OpcionMenu
 *
 *         Los Opciones Menu funcionarian como controladores los cuales son
 *         llamados para que trabajen con un modelo Y te devuelvan o a una vista
 *         o a otro controlador.
 *
 */
public class AppMenu {

	private ArrayList<OpcionMenu> opciones;
	private boolean ejecutar;

	public AppMenu() {

		opciones = new ArrayList<OpcionMenu>();
		opciones.add(new InsertarEquipo());
		opciones.add(new ModificarNacimiento());
		opciones.add(new DarBajaEquipo());
		ejecutar = true;

	}

	public void agregarOpcionMenu(OpcionMenu opcion) {

		opciones.add(opcion);

	}

	public void run() {

		while (ejecutar) {
			System.out.println("### MENU ###");
			for (int i = 0; i < opciones.size(); i++) {

				System.out.println(i + ": " + opciones.get(i).mostrar());
			}

			try {

				int opcion = new Scanner(System.in).nextInt();

				if (opcion > opciones.size() || opcion < 0) {
					System.out.println("Opcion incorrecta");
				} else {
					opciones.get(opcion).ejecutar();
				}

			} catch (InputMismatchException e) {
				System.out.println("Debe insertar un entero");
			}

		}
	}

}

package com.alvaro.corredores.aplicacion.opcionesMenu;

import java.util.Scanner;

import com.alvaro.corredores.aplicacion.OpcionMenu;
import com.alvaro.corredores.dao.DAO;
import com.alvaro.corredores.dao.DAOFactory;
import com.alvaro.corredores.modelo.Equipo;

public class DarBajaEquipo implements OpcionMenu {

	
	@Override
	public void ejecutar() {
		System.out.println("Vamos a dar de baja un equipo");
		
		System.out.println("Inserte el nombre del equipo");
		String nombre = (new Scanner(System.in)).nextLine();
		DAO<Equipo, String> dao = (new DAOFactory()).getDao(DAOFactory.HIBERNATE, Equipo.class );
		Equipo equipo = dao.findByID(nombre);
		
		if(equipo != null) {
			dao.delete(equipo);
			System.out.println("El Equipo a sido dado de baja");
		}else {
			
			System.out.println("Ese equipo no esta registrado");
		}
		
	}

	@Override
	public String mostrar() {
		// TODO Auto-generated method stub
		return "Dar de baja Equipo.";
	}

}

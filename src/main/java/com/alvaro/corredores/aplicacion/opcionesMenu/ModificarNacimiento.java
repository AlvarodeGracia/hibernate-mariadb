package com.alvaro.corredores.aplicacion.opcionesMenu;

import java.util.Scanner;

import com.alvaro.corredores.aplicacion.OpcionMenu;
import com.alvaro.corredores.aplicacion.form.Form;
import com.alvaro.corredores.aplicacion.form.Input;
import com.alvaro.corredores.aplicacion.form.Input.InputType;
import com.alvaro.corredores.dao.DAO;
import com.alvaro.corredores.dao.DAOFactory;
import com.alvaro.corredores.modelo.Corredor;

public class ModificarNacimiento implements OpcionMenu{

	
	
	@Override
	public void ejecutar() {
		System.out.println("Vamos a modificar la fecha de ancimiento de un corredor");
		
		System.out.println("Inserte el DNI del corredor");
		String dni = (new Scanner(System.in)).nextLine();
		DAO<Corredor, String> dao = (new DAOFactory()).getDao(DAOFactory.HIBERNATE, Corredor.class );
		Corredor corredor = dao.findByID(dni);
		if(corredor != null) {
			Form form = new Form(corredor);
			form.addInput(new Input("nacimiento", "Cual es la nueva fecha de ancimiento", InputType.FECHA));
			
			form.ejecutarForm();
			
			corredor = (Corredor) form.getModelo();
			
			dao.update(corredor);
		}else {
			
			System.out.println("Ese corredor no esta registrado");
			
		}
		
		
	}

	@Override
	public String mostrar() {
		// TODO Auto-generated method stub
		return "Modificar Nacimiento Corredor";
	}
	
	

}

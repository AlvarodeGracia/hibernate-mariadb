package com.alvaro.corredores.aplicacion.opcionesMenu;

import java.util.ArrayList;
import java.util.Scanner;

import com.alvaro.corredores.aplicacion.OpcionMenu;
import com.alvaro.corredores.aplicacion.form.Form;
import com.alvaro.corredores.aplicacion.form.Input;
import com.alvaro.corredores.aplicacion.form.Input.InputType;
import com.alvaro.corredores.dao.DAO;
import com.alvaro.corredores.dao.DAOFactory;
import com.alvaro.corredores.modelo.Corredor;
import com.alvaro.corredores.modelo.CorredorAmateur;
import com.alvaro.corredores.modelo.CorredorProfesional;
import com.alvaro.corredores.modelo.Equipo;

public class InsertarEquipo implements OpcionMenu {

	private Form form;

	private Equipo equipo;
	
	private ArrayList<String> dnis;

	public InsertarEquipo() {

		equipo = new Equipo();
		dnis = new ArrayList<String>();

	}

	public void ejecutar() {

		System.out.println("Insertando Equipo");

		form = new Form(equipo);
		form.addInput(new Input("nombre", "Inserte el Nombre", InputType.TEXTO));
		form.addInput(new Input("presupuesto", "Inserte el Presupuesto", InputType.DECIMAL));

		form.ejecutarForm();
		equipo = (Equipo) form.getModelo();

		int contador = 0;
		System.out.println("Puede insertar hasta 10 corredores");
		while (contador <= 10) {

			System.out.println("�Desea insertar un jugador? (Y/N");
			String opcion = (new Scanner(System.in)).nextLine();

			if (opcion.equalsIgnoreCase("y")) {

				System.out.println("Es: \n 1-amateur \n 2-profesional");
				Corredor corredor = null;
				int tipo = (new Scanner(System.in)).nextInt();
				switch (tipo) {
				case 1:
					corredor = new CorredorAmateur();
					break;
				case 2:
					corredor = new CorredorProfesional();
					break;

				}
				
				Form formCorredor = new Form(corredor, dnis);
				formCorredor.addInput(new Input("dni", "Inserte el DNI", InputType.TEXTO));
				formCorredor.addInput(new Input("nombre", "Inserte el Nombre", InputType.TEXTO));
				formCorredor.addInput(new Input("nacimiento", "Inserte La fecha de Nacimiento", InputType.FECHA));

				if (corredor instanceof CorredorAmateur) {

					formCorredor
							.addInput(new Input("lugarNacimiento", "Inserte el Lugar de Nacimiento", InputType.TEXTO));

				} else if (corredor instanceof CorredorProfesional) {

					formCorredor
							.addInput(new Input("nacionalidad", "Inserte la Nacionalidad", InputType.TEXTO));
					formCorredor
							.addInput(new Input("licencia", "Inserte la licencia", InputType.TEXTO));

				}

				formCorredor.ejecutarForm();

				corredor = (Corredor) formCorredor.getModelo();
				corredor.setEquipo(equipo);
				dnis.add(corredor.getDni());
				contador++;
				
				equipo.getCorredores().add(corredor);

				System.out.println("Corredor "+contador+" creado");

			} else {
				contador = 15;
			}

		}

		System.out.println(equipo.toString());
		
		DAO<Equipo, String> dao = (new DAOFactory<Equipo, String>()).getDao(DAOFactory.HIBERNATE, Equipo.class);
		dao.insert(equipo);
	}

	public String mostrar() {

		return "Insertar Equipo";
	}

}

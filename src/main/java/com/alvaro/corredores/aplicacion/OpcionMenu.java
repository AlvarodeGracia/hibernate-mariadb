package com.alvaro.corredores.aplicacion;

public interface OpcionMenu {

	public void ejecutar();

	public String mostrar();
}

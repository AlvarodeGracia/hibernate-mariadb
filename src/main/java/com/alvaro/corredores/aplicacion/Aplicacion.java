package com.alvaro.corredores.aplicacion;

import com.alvaro.corredores.dao.DAOFactory;
import com.alvaro.corredores.orm.Hibernate;

public class Aplicacion {
	
	public static int orm = DAOFactory.HIBERNATE;

	public static void main(String[] args) {

		System.out.println(".......Aplicacion Carrera......\n");

		AppMenu appMenu = new AppMenu();
		Hibernate.getInstance();
		appMenu.run();

	}

}

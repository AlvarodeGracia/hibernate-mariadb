package com.alvaro.corredores.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.alvaro.corredores.orm.Hibernate;

/**
 * 
 * @author Alvaro de Gracia Pastor
 *
 * @param <T> Objeto que vamos a manejar contra la BBDD
 * @param <K> Clave primaria que sera usada para manejarnos contra la BBDD
 * 
 */
public class DAOHibernate<T, K extends Serializable> implements DAO<T, K> {

	private SessionFactory sessionFactory;

	private Class<T> type;

	public DAOHibernate(Class<T> type) {

		this.type = type;
		sessionFactory = Hibernate.getInstance().getSessionFactory();

	}

	@SuppressWarnings("unchecked")
	public T findByID(K k) {

		Session session = sessionFactory.openSession();
		session.beginTransaction();
		T object = (T) session.get(type, k);
		session.getTransaction().commit();
		session.close();

		return object;

	}

	@SuppressWarnings("unchecked")
	public List<T> findAll() {
		List<T> list = new ArrayList<T>();
		Session session = sessionFactory.openSession();
		list = session.createCriteria(type).list();
		session.close();
		return null;
	}

	public int insert(T object) {

		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.save(object);
		session.getTransaction().commit();
		session.close();

		return 0;
	}

	public int update(T k) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.update(k); // <|--- Aqui guardamos el objeto en la base de datos.
		session.getTransaction().commit();
		session.close();

		return 0;
	}

	public int delete(T k) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.delete(k); // <|--- Aqui guardamos el objeto en la base de datos.
		session.getTransaction().commit();
		session.close();

		return 0;
	}

	public List<Object[]> query(String q, ArrayList<String> opciones, Integer start, Integer maxRows) {

		System.out.println("Sentencia SQL");
		Session session = sessionFactory.openSession();
		if (q == null)
			System.out.println("Null");
		Query query = session.createQuery(q);

		if (start != null) {
			query.setFirstResult(start);
		}

		if (maxRows != null)
			query.setMaxResults(maxRows);

		List<Object[]> rows = query.list();

		session.close();

		return rows;
	}

	public void close() {
		sessionFactory.close();
	}

}

package com.alvaro.corredores.dao;

import java.io.Serializable;

public class DAOFactory<T, K extends Serializable> {

	public static final int HIBERNATE = 1;

	public DAO<T, K> getDao(int tipo, Class<T> type) {

		DAO<T, K> dao = null;
		switch (tipo) {
		case 1:
			dao = new DAOHibernate<T, K>(type);
			break;
		case 2:
			break;

		}

		return dao;

	}

}

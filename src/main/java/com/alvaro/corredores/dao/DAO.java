package com.alvaro.corredores.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public interface DAO<T, Key extends Serializable> {

	public T findByID(Key k);

	public List<T> findAll();

	public int insert(T object);

	public int update(T k);

	public int delete(T k);

	public List<Object[]> query(String query, ArrayList<String> opciones, Integer start, Integer maxRows);
}
